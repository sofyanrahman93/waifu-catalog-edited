# Details

Date : 2021-11-18 13:52:27

Directory d:\workspace\economy34-vitesse

Total : 100 files,  169422 codes, 61 comments, 1494 blanks, all 170977 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [.github/FUNDING.yml](/.github/FUNDING.yml) | YAML | 1 | 0 | 1 | 2 |
| [.github/ISSUE_TEMPLATE/general.md](/.github/ISSUE_TEMPLATE/general.md) | Markdown | 9 | 0 | 4 | 13 |
| [README.md](/README.md) | Markdown | 25 | 0 | 22 | 47 |
| [index.html](/index.html) | HTML | 35 | 0 | 1 | 36 |
| [package-lock.json](/package-lock.json) | JSON | 6,298 | 0 | 1 | 6,299 |
| [package.json](/package.json) | JSON | 49 | 0 | 1 | 50 |
| [pnpm-lock.yaml](/pnpm-lock.yaml) | YAML | 3,039 | 0 | 453 | 3,492 |
| [public/favicon.svg](/public/favicon.svg) | XML | 4 | 0 | 0 | 4 |
| [src/App.vue](/src/App.vue) | vue | 11 | 0 | 1 | 12 |
| [src/components/CompanionCard.vue](/src/components/CompanionCard.vue) | vue | 191 | 0 | 23 | 214 |
| [src/components/PerkCard.vue](/src/components/PerkCard.vue) | vue | 236 | 0 | 18 | 254 |
| [src/components/README.md](/src/components/README.md) | Markdown | 5 | 0 | 5 | 10 |
| [src/components/WorldCard.vue](/src/components/WorldCard.vue) | vue | 104 | 0 | 10 | 114 |
| [src/components/basic/Button.vue](/src/components/basic/Button.vue) | vue | 35 | 0 | 5 | 40 |
| [src/components/basic/Checkbox.vue](/src/components/basic/Checkbox.vue) | vue | 29 | 0 | 3 | 32 |
| [src/components/basic/Enum.vue](/src/components/basic/Enum.vue) | vue | 49 | 0 | 3 | 52 |
| [src/components/basic/Foldable.vue](/src/components/basic/Foldable.vue) | vue | 44 | 0 | 7 | 51 |
| [src/components/basic/Input.vue](/src/components/basic/Input.vue) | vue | 48 | 0 | 3 | 51 |
| [src/components/basic/List.vue](/src/components/basic/List.vue) | vue | 35 | 0 | 4 | 39 |
| [src/components/basic/Modal.vue](/src/components/basic/Modal.vue) | vue | 35 | 0 | 3 | 38 |
| [src/components/basic/NumberInput.vue](/src/components/basic/NumberInput.vue) | vue | 51 | 0 | 8 | 59 |
| [src/components/basic/ProgressBar.vue](/src/components/basic/ProgressBar.vue) | vue | 22 | 0 | 5 | 27 |
| [src/components/basic/Select.vue](/src/components/basic/Select.vue) | vue | 61 | 0 | 4 | 65 |
| [src/components/basic/SortButton.vue](/src/components/basic/SortButton.vue) | vue | 0 | 0 | 1 | 1 |
| [src/components/basic/Table.vue](/src/components/basic/Table.vue) | vue | 37 | 0 | 3 | 40 |
| [src/components/basic/TextArea.vue](/src/components/basic/TextArea.vue) | vue | 45 | 0 | 4 | 49 |
| [src/components/basic/Valid.vue](/src/components/basic/Valid.vue) | vue | 12 | 0 | 2 | 14 |
| [src/components/big/Footer.vue](/src/components/big/Footer.vue) | vue | 219 | 0 | 27 | 246 |
| [src/components/big/Header.vue](/src/components/big/Header.vue) | vue | 24 | 0 | 7 | 31 |
| [src/components/big/Navigation.vue](/src/components/big/Navigation.vue) | vue | 77 | 0 | 10 | 87 |
| [src/components/big/SideApps.vue](/src/components/big/SideApps.vue) | vue | 60 | 0 | 8 | 68 |
| [src/components/big/SideMenu.vue](/src/components/big/SideMenu.vue) | vue | 79 | 0 | 6 | 85 |
| [src/components/big/footer/Apps.vue](/src/components/big/footer/Apps.vue) | vue | 44 | 0 | 7 | 51 |
| [src/components/big/footer/Build.vue](/src/components/big/footer/Build.vue) | vue | 207 | 1 | 8 | 216 |
| [src/components/big/footer/Manual.vue](/src/components/big/footer/Manual.vue) | vue | 24 | 0 | 4 | 28 |
| [src/components/big/footer/Spendings.vue](/src/components/big/footer/Spendings.vue) | vue | 63 | 0 | 4 | 67 |
| [src/components/big/footer/apps/CashRules.vue](/src/components/big/footer/apps/CashRules.vue) | vue | 7 | 0 | 2 | 9 |
| [src/components/big/footer/apps/DiceMachine.vue](/src/components/big/footer/apps/DiceMachine.vue) | vue | 53 | 0 | 8 | 61 |
| [src/components/big/footer/apps/ExitStage.vue](/src/components/big/footer/apps/ExitStage.vue) | vue | 69 | 0 | 10 | 79 |
| [src/components/big/footer/apps/Missions.vue](/src/components/big/footer/apps/Missions.vue) | vue | 7 | 0 | 2 | 9 |
| [src/components/big/footer/apps/RandomBuild.vue](/src/components/big/footer/apps/RandomBuild.vue) | vue | 34 | 0 | 9 | 43 |
| [src/components/big/footer/apps/RandomChar.vue](/src/components/big/footer/apps/RandomChar.vue) | vue | 28 | 0 | 7 | 35 |
| [src/components/big/footer/apps/RandomWorld.vue](/src/components/big/footer/apps/RandomWorld.vue) | vue | 31 | 0 | 7 | 38 |
| [src/components/modals/AddCharacter.vue](/src/components/modals/AddCharacter.vue) | vue | 108 | 0 | 16 | 124 |
| [src/components/modals/AddRide.vue](/src/components/modals/AddRide.vue) | vue | 170 | 8 | 17 | 195 |
| [src/components/modals/AddWorld.vue](/src/components/modals/AddWorld.vue) | vue | 140 | 0 | 12 | 152 |
| [src/components/modals/RitualCircle.vue](/src/components/modals/RitualCircle.vue) | vue | 111 | 0 | 12 | 123 |
| [src/components/modals/SaveLoad.vue](/src/components/modals/SaveLoad.vue) | vue | 112 | 0 | 11 | 123 |
| [src/components/modals/Share.vue](/src/components/modals/Share.vue) | vue | 65 | 0 | 8 | 73 |
| [src/components/modals/ShareLoad.vue](/src/components/modals/ShareLoad.vue) | vue | 59 | 0 | 7 | 66 |
| [src/components/modals/SlightlyUsed.vue](/src/components/modals/SlightlyUsed.vue) | vue | 99 | 0 | 8 | 107 |
| [src/components/small/AnythingInput.vue](/src/components/small/AnythingInput.vue) | vue | 107 | 0 | 14 | 121 |
| [src/components/small/CharacterInput.vue](/src/components/small/CharacterInput.vue) | vue | 77 | 0 | 11 | 88 |
| [src/components/small/Desc.vue](/src/components/small/Desc.vue) | vue | 36 | 0 | 8 | 44 |
| [src/components/small/ModeButton.vue](/src/components/small/ModeButton.vue) | vue | 28 | 0 | 5 | 33 |
| [src/data/binding.ts](/src/data/binding.ts) | TypeScript | 717 | 5 | 63 | 785 |
| [src/data/characters.json](/src/data/characters.json) | JSON | 147,846 | 0 | 2 | 147,848 |
| [src/data/characters3.json](/src/data/characters3.json) | JSON | 3 | 0 | 0 | 3 |
| [src/data/constants.ts](/src/data/constants.ts) | TypeScript | 127 | 0 | 13 | 140 |
| [src/data/heritage.ts](/src/data/heritage.ts) | TypeScript | 591 | 1 | 36 | 628 |
| [src/data/intensity.ts](/src/data/intensity.ts) | TypeScript | 152 | 0 | 3 | 155 |
| [src/data/origin.ts](/src/data/origin.ts) | TypeScript | 115 | 0 | 8 | 123 |
| [src/data/rules.ts](/src/data/rules.ts) | TypeScript | 202 | 0 | 27 | 229 |
| [src/data/talents.ts](/src/data/talents.ts) | TypeScript | 962 | 22 | 137 | 1,121 |
| [src/data/userCharacters.json](/src/data/userCharacters.json) | JSON | 255 | 0 | 1 | 256 |
| [src/data/userWorlds.json](/src/data/userWorlds.json) | JSON | 311 | 0 | 0 | 311 |
| [src/data/waifu_perks.ts](/src/data/waifu_perks.ts) | TypeScript | 1,099 | 0 | 5 | 1,104 |
| [src/data/worlds.json](/src/data/worlds.json) | JSON | 1,472 | 0 | 0 | 1,472 |
| [src/logic/index.ts](/src/logic/index.ts) | TypeScript | 53 | 0 | 9 | 62 |
| [src/logic/misc.ts](/src/logic/misc.ts) | TypeScript | 94 | 1 | 7 | 102 |
| [src/logic/toggles.ts](/src/logic/toggles.ts) | TypeScript | 21 | 0 | 4 | 25 |
| [src/main.ts](/src/main.ts) | TypeScript | 31 | 3 | 4 | 38 |
| [src/pages/README.md](/src/pages/README.md) | Markdown | 13 | 0 | 8 | 21 |
| [src/pages/[...all].vue](/src/pages/[...all].vue) | vue | 5 | 0 | 1 | 6 |
| [src/pages/about.vue](/src/pages/about.vue) | vue | 41 | 0 | 1 | 42 |
| [src/pages/binding/index.vue](/src/pages/binding/index.vue) | vue | 298 | 0 | 28 | 326 |
| [src/pages/changelog.vue](/src/pages/changelog.vue) | vue | 122 | 0 | 3 | 125 |
| [src/pages/companions/index.vue](/src/pages/companions/index.vue) | vue | 226 | 0 | 23 | 249 |
| [src/pages/credits.vue](/src/pages/credits.vue) | vue | 39 | 0 | 4 | 43 |
| [src/pages/everything.vue](/src/pages/everything.vue) | vue | 104 | 0 | 20 | 124 |
| [src/pages/help.vue](/src/pages/help.vue) | vue | 45 | 0 | 2 | 47 |
| [src/pages/heritage/index.vue](/src/pages/heritage/index.vue) | vue | 152 | 0 | 14 | 166 |
| [src/pages/index.vue](/src/pages/index.vue) | vue | 132 | 0 | 10 | 142 |
| [src/pages/intensity/index.vue](/src/pages/intensity/index.vue) | vue | 91 | 0 | 9 | 100 |
| [src/pages/origin/index.vue](/src/pages/origin/index.vue) | vue | 106 | 0 | 10 | 116 |
| [src/pages/talents.vue](/src/pages/talents.vue) | vue | 20 | 0 | 3 | 23 |
| [src/pages/talents/defense.vue](/src/pages/talents/defense.vue) | vue | 67 | 0 | 8 | 75 |
| [src/pages/talents/home.vue](/src/pages/talents/home.vue) | vue | 68 | 0 | 8 | 76 |
| [src/pages/talents/perks.vue](/src/pages/talents/perks.vue) | vue | 53 | 0 | 9 | 62 |
| [src/pages/talents/ride.vue](/src/pages/talents/ride.vue) | vue | 177 | 0 | 14 | 191 |
| [src/pages/talents/specific.vue](/src/pages/talents/specific.vue) | vue | 154 | 0 | 17 | 171 |
| [src/pages/talents/talent.vue](/src/pages/talents/talent.vue) | vue | 66 | 7 | 11 | 84 |
| [src/pages/world/index.vue](/src/pages/world/index.vue) | vue | 127 | 0 | 15 | 142 |
| [src/store/play.ts](/src/store/play.ts) | TypeScript | 10 | 0 | 4 | 14 |
| [src/store/saves.ts](/src/store/saves.ts) | TypeScript | 14 | 0 | 3 | 17 |
| [src/store/store.ts](/src/store/store.ts) | TypeScript | 243 | 0 | 45 | 288 |
| [src/styles/main.css](/src/styles/main.css) | PostCSS | 233 | 0 | 40 | 273 |
| [tsconfig.json](/tsconfig.json) | JSON | 21 | 5 | 0 | 26 |
| [vite.config.ts](/vite.config.ts) | TypeScript | 66 | 7 | 12 | 85 |
| [windi.config.ts](/windi.config.ts) | TypeScript | 30 | 1 | 3 | 34 |

[summary](results.md)
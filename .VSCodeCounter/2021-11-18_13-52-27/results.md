# Summary

Date : 2021-11-18 13:52:27

Directory d:\workspace\economy34-vitesse

Total : 100 files,  169422 codes, 61 comments, 1494 blanks, all 170977 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 8 | 156,255 | 5 | 5 | 156,265 |
| vue | 66 | 5,276 | 16 | 572 | 5,864 |
| TypeScript | 17 | 4,527 | 40 | 383 | 4,950 |
| YAML | 2 | 3,040 | 0 | 454 | 3,494 |
| PostCSS | 1 | 233 | 0 | 40 | 273 |
| Markdown | 4 | 52 | 0 | 39 | 91 |
| HTML | 1 | 35 | 0 | 1 | 36 |
| XML | 1 | 4 | 0 | 0 | 4 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 100 | 169,422 | 61 | 1,494 | 170,977 |
| .github | 2 | 10 | 0 | 5 | 15 |
| .github\ISSUE_TEMPLATE | 1 | 9 | 0 | 4 | 13 |
| public | 1 | 4 | 0 | 0 | 4 |
| src | 89 | 159,845 | 48 | 996 | 160,889 |
| src\components | 46 | 3,177 | 9 | 366 | 3,552 |
| src\components\basic | 14 | 503 | 0 | 55 | 558 |
| src\components\big | 16 | 1,026 | 1 | 126 | 1,153 |
| src\components\big\footer | 11 | 567 | 1 | 68 | 636 |
| src\components\big\footer\apps | 7 | 229 | 0 | 45 | 274 |
| src\components\modals | 8 | 864 | 8 | 91 | 963 |
| src\components\small | 4 | 248 | 0 | 38 | 286 |
| src\data | 13 | 153,852 | 28 | 295 | 154,175 |
| src\logic | 3 | 168 | 1 | 20 | 189 |
| src\pages | 21 | 2,106 | 7 | 218 | 2,331 |
| src\pages\binding | 1 | 298 | 0 | 28 | 326 |
| src\pages\companions | 1 | 226 | 0 | 23 | 249 |
| src\pages\heritage | 1 | 152 | 0 | 14 | 166 |
| src\pages\intensity | 1 | 91 | 0 | 9 | 100 |
| src\pages\origin | 1 | 106 | 0 | 10 | 116 |
| src\pages\talents | 6 | 585 | 7 | 67 | 659 |
| src\pages\world | 1 | 127 | 0 | 15 | 142 |
| src\store | 3 | 267 | 0 | 52 | 319 |
| src\styles | 1 | 233 | 0 | 40 | 273 |

[details](details.md)
# Summary

Date : 2022-01-08 22:31:00

Directory d:\workspace\economy34-vitesse

Total : 133 files,  342693 codes, 77 comments, 1882 blanks, all 344652 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 8 | 325,709 | 5 | 4 | 325,718 |
| vue | 91 | 7,494 | 9 | 812 | 8,315 |
| TypeScript | 25 | 6,301 | 62 | 559 | 6,922 |
| YAML | 2 | 3,040 | 0 | 454 | 3,494 |
| PostCSS | 1 | 58 | 1 | 13 | 72 |
| Markdown | 4 | 52 | 0 | 39 | 91 |
| HTML | 1 | 35 | 0 | 1 | 36 |
| XML | 1 | 4 | 0 | 0 | 4 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 133 | 342,693 | 77 | 1,882 | 344,652 |
| .github | 2 | 10 | 0 | 5 | 15 |
| .github\ISSUE_TEMPLATE | 1 | 9 | 0 | 4 | 13 |
| public | 1 | 4 | 0 | 0 | 4 |
| src | 121 | 333,099 | 64 | 1,382 | 334,545 |
| src\components | 69 | 5,203 | 9 | 604 | 5,816 |
| src\components\basic | 17 | 783 | 0 | 92 | 875 |
| src\components\big | 23 | 1,932 | 1 | 231 | 2,164 |
| src\components\big\footer | 18 | 1,459 | 1 | 176 | 1,636 |
| src\components\big\footer\apps | 13 | 917 | 0 | 132 | 1,049 |
| src\components\modals | 15 | 1,249 | 8 | 141 | 1,398 |
| src\components\modals\dialogs | 3 | 88 | 0 | 14 | 102 |
| src\components\small | 7 | 388 | 0 | 57 | 445 |
| src\data | 16 | 324,338 | 37 | 384 | 324,759 |
| src\logic | 5 | 655 | 14 | 74 | 743 |
| src\pages | 23 | 2,279 | 0 | 215 | 2,494 |
| src\pages\binding | 1 | 212 | 0 | 24 | 236 |
| src\pages\companions | 1 | 294 | 0 | 31 | 325 |
| src\pages\heritage | 1 | 132 | 0 | 16 | 148 |
| src\pages\intensity | 1 | 73 | 0 | 8 | 81 |
| src\pages\origin | 1 | 114 | 0 | 11 | 125 |
| src\pages\talents | 6 | 433 | 0 | 50 | 483 |
| src\pages\world | 1 | 127 | 0 | 15 | 142 |
| src\store | 5 | 505 | 0 | 82 | 587 |
| src\styles | 1 | 58 | 1 | 13 | 72 |

[details](details.md)
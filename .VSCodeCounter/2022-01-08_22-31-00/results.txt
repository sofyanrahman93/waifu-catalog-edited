Date : 2022-01-08 22:31:00
Directory : d:\workspace\economy34-vitesse
Total : 133 files,  342693 codes, 77 comments, 1882 blanks, all 344652 lines

Languages
+------------+------------+------------+------------+------------+------------+
| language   | files      | code       | comment    | blank      | total      |
+------------+------------+------------+------------+------------+------------+
| JSON       |          8 |    325,709 |          5 |          4 |    325,718 |
| vue        |         91 |      7,494 |          9 |        812 |      8,315 |
| TypeScript |         25 |      6,301 |         62 |        559 |      6,922 |
| YAML       |          2 |      3,040 |          0 |        454 |      3,494 |
| PostCSS    |          1 |         58 |          1 |         13 |         72 |
| Markdown   |          4 |         52 |          0 |         39 |         91 |
| HTML       |          1 |         35 |          0 |          1 |         36 |
| XML        |          1 |          4 |          0 |          0 |          4 |
+------------+------------+------------+------------+------------+------------+

Directories
+-----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                              | files      | code       | comment    | blank      | total      |
+-----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                 |        133 |    342,693 |         77 |      1,882 |    344,652 |
| .github                                                                           |          2 |         10 |          0 |          5 |         15 |
| .github\ISSUE_TEMPLATE                                                            |          1 |          9 |          0 |          4 |         13 |
| public                                                                            |          1 |          4 |          0 |          0 |          4 |
| src                                                                               |        121 |    333,099 |         64 |      1,382 |    334,545 |
| src\components                                                                    |         69 |      5,203 |          9 |        604 |      5,816 |
| src\components\basic                                                              |         17 |        783 |          0 |         92 |        875 |
| src\components\big                                                                |         23 |      1,932 |          1 |        231 |      2,164 |
| src\components\big\footer                                                         |         18 |      1,459 |          1 |        176 |      1,636 |
| src\components\big\footer\apps                                                    |         13 |        917 |          0 |        132 |      1,049 |
| src\components\modals                                                             |         15 |      1,249 |          8 |        141 |      1,398 |
| src\components\modals\dialogs                                                     |          3 |         88 |          0 |         14 |        102 |
| src\components\small                                                              |          7 |        388 |          0 |         57 |        445 |
| src\data                                                                          |         16 |    324,338 |         37 |        384 |    324,759 |
| src\logic                                                                         |          5 |        655 |         14 |         74 |        743 |
| src\pages                                                                         |         23 |      2,279 |          0 |        215 |      2,494 |
| src\pages\binding                                                                 |          1 |        212 |          0 |         24 |        236 |
| src\pages\companions                                                              |          1 |        294 |          0 |         31 |        325 |
| src\pages\heritage                                                                |          1 |        132 |          0 |         16 |        148 |
| src\pages\intensity                                                               |          1 |         73 |          0 |          8 |         81 |
| src\pages\origin                                                                  |          1 |        114 |          0 |         11 |        125 |
| src\pages\talents                                                                 |          6 |        433 |          0 |         50 |        483 |
| src\pages\world                                                                   |          1 |        127 |          0 |         15 |        142 |
| src\store                                                                         |          5 |        505 |          0 |         82 |        587 |
| src\styles                                                                        |          1 |         58 |          1 |         13 |         72 |
+-----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+-----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| filename                                                                          | language   | code       | comment    | blank      | total      |
+-----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| d:\workspace\economy34-vitesse\.github\FUNDING.yml                                | YAML       |          1 |          0 |          1 |          2 |
| d:\workspace\economy34-vitesse\.github\ISSUE_TEMPLATE\general.md                  | Markdown   |          9 |          0 |          4 |         13 |
| d:\workspace\economy34-vitesse\README.md                                          | Markdown   |         25 |          0 |         22 |         47 |
| d:\workspace\economy34-vitesse\global.d.ts                                        | TypeScript |         17 |          0 |          2 |         19 |
| d:\workspace\economy34-vitesse\index.html                                         | HTML       |         35 |          0 |          1 |         36 |
| d:\workspace\economy34-vitesse\package-lock.json                                  | JSON       |      6,298 |          0 |          1 |      6,299 |
| d:\workspace\economy34-vitesse\package.json                                       | JSON       |         49 |          0 |          1 |         50 |
| d:\workspace\economy34-vitesse\pnpm-lock.yaml                                     | YAML       |      3,039 |          0 |        453 |      3,492 |
| d:\workspace\economy34-vitesse\public\favicon.svg                                 | XML        |          4 |          0 |          0 |          4 |
| d:\workspace\economy34-vitesse\src\App.vue                                        | vue        |         30 |          0 |          6 |         36 |
| d:\workspace\economy34-vitesse\src\components\ChallengeCard.vue                   | vue        |         62 |          0 |          7 |         69 |
| d:\workspace\economy34-vitesse\src\components\CompanionCard.vue                   | vue        |        265 |          0 |         25 |        290 |
| d:\workspace\economy34-vitesse\src\components\CompanionCardMini.vue               | vue        |        109 |          0 |          7 |        116 |
| d:\workspace\economy34-vitesse\src\components\PerkCard.vue                        | vue        |        233 |          0 |         17 |        250 |
| d:\workspace\economy34-vitesse\src\components\README.md                           | Markdown   |          5 |          0 |          5 |         10 |
| d:\workspace\economy34-vitesse\src\components\WorldCard.vue                       | vue        |        114 |          0 |         12 |        126 |
| d:\workspace\economy34-vitesse\src\components\YourCardMini.vue                    | vue        |         63 |          0 |         10 |         73 |
| d:\workspace\economy34-vitesse\src\components\basic\Button.vue                    | vue        |         40 |          0 |          5 |         45 |
| d:\workspace\economy34-vitesse\src\components\basic\Checkbox.vue                  | vue        |         25 |          0 |          3 |         28 |
| d:\workspace\economy34-vitesse\src\components\basic\Enum.vue                      | vue        |         76 |          0 |          6 |         82 |
| d:\workspace\economy34-vitesse\src\components\basic\Foldable.vue                  | vue        |         34 |          0 |          6 |         40 |
| d:\workspace\economy34-vitesse\src\components\basic\Input.vue                     | vue        |         49 |          0 |          3 |         52 |
| d:\workspace\economy34-vitesse\src\components\basic\List.vue                      | vue        |         35 |          0 |          4 |         39 |
| d:\workspace\economy34-vitesse\src\components\basic\Modal.vue                     | vue        |         32 |          0 |          3 |         35 |
| d:\workspace\economy34-vitesse\src\components\basic\NumberInput.vue               | vue        |        103 |          0 |         15 |        118 |
| d:\workspace\economy34-vitesse\src\components\basic\ProgressBar.vue               | vue        |         22 |          0 |          5 |         27 |
| d:\workspace\economy34-vitesse\src\components\basic\Select.vue                    | vue        |         61 |          0 |          4 |         65 |
| d:\workspace\economy34-vitesse\src\components\basic\SortButton.vue                | vue        |          0 |          0 |          1 |          1 |
| d:\workspace\economy34-vitesse\src\components\basic\Table.vue                     | vue        |         34 |          0 |          4 |         38 |
| d:\workspace\economy34-vitesse\src\components\basic\TagInput.vue                  | vue        |        111 |          0 |         12 |        123 |
| d:\workspace\economy34-vitesse\src\components\basic\TextArea.vue                  | vue        |         41 |          0 |          3 |         44 |
| d:\workspace\economy34-vitesse\src\components\basic\Toggle.vue                    | vue        |         36 |          0 |          6 |         42 |
| d:\workspace\economy34-vitesse\src\components\basic\Valid.vue                     | vue        |         12 |          0 |          2 |         14 |
| d:\workspace\economy34-vitesse\src\components\basic\Variants.vue                  | vue        |         72 |          0 |         10 |         82 |
| d:\workspace\economy34-vitesse\src\components\big\Footer.vue                      | vue        |        195 |          0 |         27 |        222 |
| d:\workspace\economy34-vitesse\src\components\big\Header.vue                      | vue        |         28 |          0 |          6 |         34 |
| d:\workspace\economy34-vitesse\src\components\big\Navigation.vue                  | vue        |         67 |          0 |          8 |         75 |
| d:\workspace\economy34-vitesse\src\components\big\SideApps.vue                    | vue        |         59 |          0 |          8 |         67 |
| d:\workspace\economy34-vitesse\src\components\big\SideMenu.vue                    | vue        |        124 |          0 |          6 |        130 |
| d:\workspace\economy34-vitesse\src\components\big\footer\Apps.vue                 | vue        |         62 |          0 |         10 |         72 |
| d:\workspace\economy34-vitesse\src\components\big\footer\Build.vue                | vue        |        272 |          1 |         10 |        283 |
| d:\workspace\economy34-vitesse\src\components\big\footer\Companions.vue           | vue        |        100 |          0 |         15 |        115 |
| d:\workspace\economy34-vitesse\src\components\big\footer\Manual.vue               | vue        |         24 |          0 |          4 |         28 |
| d:\workspace\economy34-vitesse\src\components\big\footer\Spendings.vue            | vue        |         84 |          0 |          5 |         89 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\BulkCapture.vue     | vue        |         43 |          0 |          5 |         48 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\CashRules.vue       | vue        |         56 |          0 |          7 |         63 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\DiceMachine.vue     | vue        |         53 |          0 |          8 |         61 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\DragonAura.vue      | vue        |         41 |          0 |          6 |         47 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\ExitStage.vue       | vue        |        146 |          0 |         15 |        161 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\Missions.vue        | vue        |         58 |          0 |          8 |         66 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\Notes.vue           | vue        |         26 |          0 |          4 |         30 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\PocketSpace.vue     | vue        |         54 |          0 |         17 |         71 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\RandomBuild.vue     | vue        |        195 |          0 |         34 |        229 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\RandomChar.vue      | vue        |         24 |          0 |          5 |         29 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\RandomWorld.vue     | vue        |         24 |          0 |          5 |         29 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\RitualCircleApp.vue | vue        |        138 |          0 |         10 |        148 |
| d:\workspace\economy34-vitesse\src\components\big\footer\apps\UserBuilds.vue      | vue        |         59 |          0 |          8 |         67 |
| d:\workspace\economy34-vitesse\src\components\modals\AddCharacter.vue             | vue        |        179 |          0 |         19 |        198 |
| d:\workspace\economy34-vitesse\src\components\modals\AddMission.vue               | vue        |        138 |          0 |         10 |        148 |
| d:\workspace\economy34-vitesse\src\components\modals\AddPerk.vue                  | vue        |         86 |          0 |         11 |         97 |
| d:\workspace\economy34-vitesse\src\components\modals\AddRide.vue                  | vue        |        170 |          8 |         17 |        195 |
| d:\workspace\economy34-vitesse\src\components\modals\AddWorld.vue                 | vue        |        121 |          0 |         12 |        133 |
| d:\workspace\economy34-vitesse\src\components\modals\RitualCircle.vue             | vue        |        111 |          0 |         12 |        123 |
| d:\workspace\economy34-vitesse\src\components\modals\SaveLoad.vue                 | vue        |         97 |          0 |         13 |        110 |
| d:\workspace\economy34-vitesse\src\components\modals\Share.vue                    | vue        |         41 |          0 |          7 |         48 |
| d:\workspace\economy34-vitesse\src\components\modals\ShareLoad.vue                | vue        |         35 |          0 |          6 |         41 |
| d:\workspace\economy34-vitesse\src\components\modals\ShroudElements.vue           | vue        |         53 |          0 |          7 |         60 |
| d:\workspace\economy34-vitesse\src\components\modals\SlightlyUsed.vue             | vue        |         99 |          0 |          8 |        107 |
| d:\workspace\economy34-vitesse\src\components\modals\Tags.vue                     | vue        |         31 |          0 |          5 |         36 |
| d:\workspace\economy34-vitesse\src\components\modals\dialogs\ConfirmDialog.vue    | vue        |         25 |          0 |          5 |         30 |
| d:\workspace\economy34-vitesse\src\components\modals\dialogs\InfoDialog.vue       | vue        |         17 |          0 |          4 |         21 |
| d:\workspace\economy34-vitesse\src\components\modals\dialogs\PromoteDialog.vue    | vue        |         46 |          0 |          5 |         51 |
| d:\workspace\economy34-vitesse\src\components\small\AnythingInput.vue             | vue        |        105 |          0 |         13 |        118 |
| d:\workspace\economy34-vitesse\src\components\small\CharacterInput.vue            | vue        |         78 |          0 |         11 |         89 |
| d:\workspace\economy34-vitesse\src\components\small\Desc.vue                      | vue        |         27 |          0 |          8 |         35 |
| d:\workspace\economy34-vitesse\src\components\small\EditableProgressbar.vue       | vue        |         50 |          0 |          8 |         58 |
| d:\workspace\economy34-vitesse\src\components\small\InputWithSearch.vue           | vue        |         75 |          0 |          9 |         84 |
| d:\workspace\economy34-vitesse\src\components\small\MiniWorldCard.vue             | vue        |         34 |          0 |          5 |         39 |
| d:\workspace\economy34-vitesse\src\components\small\ModeButton.vue                | vue        |         19 |          0 |          3 |         22 |
| d:\workspace\economy34-vitesse\src\data\DLCs.ts                                   | TypeScript |        504 |         10 |         63 |        577 |
| d:\workspace\economy34-vitesse\src\data\binding.ts                                | TypeScript |        740 |          5 |         64 |        809 |
| d:\workspace\economy34-vitesse\src\data\challenges.ts                             | TypeScript |        112 |          0 |          5 |        117 |
| d:\workspace\economy34-vitesse\src\data\characters.json                           | JSON       |    143,918 |          0 |          0 |    143,918 |
| d:\workspace\economy34-vitesse\src\data\charactersOld.json                        | JSON       |    148,144 |          0 |          0 |    148,144 |
| d:\workspace\economy34-vitesse\src\data\constants.ts                              | TypeScript |        239 |          0 |         29 |        268 |
| d:\workspace\economy34-vitesse\src\data\heritage.ts                               | TypeScript |        592 |          0 |         36 |        628 |
| d:\workspace\economy34-vitesse\src\data\intensity.ts                              | TypeScript |        165 |          0 |          3 |        168 |
| d:\workspace\economy34-vitesse\src\data\missions.ts                               | TypeScript |        222 |          0 |          2 |        224 |
| d:\workspace\economy34-vitesse\src\data\origin.ts                                 | TypeScript |        116 |          0 |          8 |        124 |
| d:\workspace\economy34-vitesse\src\data\rules.ts                                  | TypeScript |        224 |          0 |         27 |        251 |
| d:\workspace\economy34-vitesse\src\data\talents.ts                                | TypeScript |        964 |         21 |        137 |      1,122 |
| d:\workspace\economy34-vitesse\src\data\userCharacters.json                       | JSON       |     25,448 |          0 |          0 |     25,448 |
| d:\workspace\economy34-vitesse\src\data\userWorlds.json                           | JSON       |        357 |          0 |          2 |        359 |
| d:\workspace\economy34-vitesse\src\data\waifu_perks.ts                            | TypeScript |      1,119 |          1 |          8 |      1,128 |
| d:\workspace\economy34-vitesse\src\data\worlds.json                               | JSON       |      1,474 |          0 |          0 |      1,474 |
| d:\workspace\economy34-vitesse\src\logic\dialog.ts                                | TypeScript |         22 |          0 |          5 |         27 |
| d:\workspace\economy34-vitesse\src\logic\index.ts                                 | TypeScript |         78 |          0 |         12 |         90 |
| d:\workspace\economy34-vitesse\src\logic\misc.ts                                  | TypeScript |         91 |          0 |          8 |         99 |
| d:\workspace\economy34-vitesse\src\logic\perksLogic.ts                            | TypeScript |        431 |         14 |         42 |        487 |
| d:\workspace\economy34-vitesse\src\logic\toggles.ts                               | TypeScript |         33 |          0 |          7 |         40 |
| d:\workspace\economy34-vitesse\src\main.ts                                        | TypeScript |         31 |          3 |          4 |         38 |
| d:\workspace\economy34-vitesse\src\pages\README.md                                | Markdown   |         13 |          0 |          8 |         21 |
| d:\workspace\economy34-vitesse\src\pages\[...all].vue                             | vue        |          5 |          0 |          1 |          6 |
| d:\workspace\economy34-vitesse\src\pages\about.vue                                | vue        |         53 |          0 |          1 |         54 |
| d:\workspace\economy34-vitesse\src\pages\binding\index.vue                        | vue        |        212 |          0 |         24 |        236 |
| d:\workspace\economy34-vitesse\src\pages\challenges.vue                           | vue        |         41 |          0 |          7 |         48 |
| d:\workspace\economy34-vitesse\src\pages\changelog.vue                            | vue        |        338 |          0 |          3 |        341 |
| d:\workspace\economy34-vitesse\src\pages\companions\index.vue                     | vue        |        294 |          0 |         31 |        325 |
| d:\workspace\economy34-vitesse\src\pages\credits.vue                              | vue        |         37 |          0 |          2 |         39 |
| d:\workspace\economy34-vitesse\src\pages\everything.vue                           | vue        |        110 |          0 |         21 |        131 |
| d:\workspace\economy34-vitesse\src\pages\help.vue                                 | vue        |         82 |          0 |          2 |         84 |
| d:\workspace\economy34-vitesse\src\pages\heritage\index.vue                       | vue        |        132 |          0 |         16 |        148 |
| d:\workspace\economy34-vitesse\src\pages\index.vue                                | vue        |        139 |          0 |         10 |        149 |
| d:\workspace\economy34-vitesse\src\pages\intensity\index.vue                      | vue        |         73 |          0 |          8 |         81 |
| d:\workspace\economy34-vitesse\src\pages\missions.vue                             | vue        |         58 |          0 |          2 |         60 |
| d:\workspace\economy34-vitesse\src\pages\origin\index.vue                         | vue        |        114 |          0 |         11 |        125 |
| d:\workspace\economy34-vitesse\src\pages\talents.vue                              | vue        |         18 |          0 |          3 |         21 |
| d:\workspace\economy34-vitesse\src\pages\talents\defense.vue                      | vue        |         37 |          0 |          6 |         43 |
| d:\workspace\economy34-vitesse\src\pages\talents\home.vue                         | vue        |         38 |          0 |          7 |         45 |
| d:\workspace\economy34-vitesse\src\pages\talents\perks.vue                        | vue        |         27 |          0 |          6 |         33 |
| d:\workspace\economy34-vitesse\src\pages\talents\ride.vue                         | vue        |        156 |          0 |         13 |        169 |
| d:\workspace\economy34-vitesse\src\pages\talents\specific.vue                     | vue        |        134 |          0 |         10 |        144 |
| d:\workspace\economy34-vitesse\src\pages\talents\talent.vue                       | vue        |         41 |          0 |          8 |         49 |
| d:\workspace\economy34-vitesse\src\pages\world\index.vue                          | vue        |        127 |          0 |         15 |        142 |
| d:\workspace\economy34-vitesse\src\store\challenges.ts                            | TypeScript |         10 |          0 |          3 |         13 |
| d:\workspace\economy34-vitesse\src\store\chargen.ts                               | TypeScript |        145 |          0 |         31 |        176 |
| d:\workspace\economy34-vitesse\src\store\play.ts                                  | TypeScript |         20 |          0 |          5 |         25 |
| d:\workspace\economy34-vitesse\src\store\saves.ts                                 | TypeScript |         70 |          0 |          9 |         79 |
| d:\workspace\economy34-vitesse\src\store\store.ts                                 | TypeScript |        260 |          0 |         34 |        294 |
| d:\workspace\economy34-vitesse\src\styles\main.css                                | PostCSS    |         58 |          1 |         13 |         72 |
| d:\workspace\economy34-vitesse\tsconfig.json                                      | JSON       |         21 |          5 |          0 |         26 |
| d:\workspace\economy34-vitesse\vite.config.ts                                     | TypeScript |         66 |          7 |         12 |         85 |
| d:\workspace\economy34-vitesse\windi.config.ts                                    | TypeScript |         30 |          1 |          3 |         34 |
| Total                                                                             |            |    342,693 |         77 |      1,882 |    344,652 |
+-----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
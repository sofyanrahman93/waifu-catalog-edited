export const missions = [
  {
    source: '',
    title: 'Uplift to Fight',
    scope: 'Grand',
    rewards: [{ value: 'Exit Stage Left and All Add ons, 1 T11 Ticket' }, { value: 'Bonus 300 credits at chargin' }],
    requirements: [{ value: 'Defeat the Reapers' }, { value: 'May start at any point from the onset of the Rachni Wars, to Shanxi' }],
    author: 'Templar99',
    desc: 'There is no Crucible. There is no Deus ex Machina or Macguffin. There is no programming trick, or clever ploy to end the war in one move. You must either defeat the Reapers, or die with the Galaxy.\nYou must prepare accordingly.\n',
    loca: 'Mass Effect',
    image: '',
  },
  {
    author: 'Testing',
    title: 'Capture all Waifus',
    scope: 'Standard',
    image: '',
    loca: 'Disgaea 5',
    rewards: [{ value: '+400 Credits' }, { value: 'Template stacking 0 + Template stacking I' }],
    requirements: [{ value: 'Time limit:3 months' }],
    desc: 'Capture all Waifus in the Disgaea World within the Timelimit given.',
    source: '',
  },
  {
    rewards: [{ value: 'Exit Stage Left and all upgrades, 1 T11 tickets and 2000 credits' },
      { value: '2 T11 Tickets' }],
    source: '',
    image: '',
    loca: 'DC',
    desc: 'Destroy the true multiversal Darkseid and Apokolips within the 4th world.\nMay not leave the DC multiverse until the mission is concluded.\nStarting bonus of double chargin credits\nDarkseid will expect a threat, but will not know about you directly. He will be looking for said threat everywhere',
    scope: 'Grand',
    author: 'Templar99',
    title: 'Destroy Apokolips',
    requirements: [{ value: 'Destroy Apokoliops' }, { value: 'Destroy (NOT capture) Darkseid ' }],
  },
  {
    image: 'https://static.wikia.nocookie.net/onepiece/images/2/24/Gol_D._Roger_Anime_Infobox.png',
    source: '',
    scope: 'Grand',
    loca: 'One Piece',
    title: 'The Pirate King',
    requirements: [{ value: 'Reach the island of Raftel' }, { value: 'Become recognized as the Pirate King by the general public' }],
    rewards: [{ value: '500 Credits' }, { value: 'Any vehicle from the Ride section. Anyone considered to be a crew member has the effects of Catch a Ride Training' }],
    desc: 'You must become the Pirate King, greatest pirate in all the blues',
    author: 'Trismegistus',
  },
  {
    image: 'https://static.wikia.nocookie.net/onepiece/images/4/44/Skypiea_Infobox.png',
    scope: 'Standard',
    title: 'Ruler of the Skies',
    rewards: [{ value: '100 Credits' }, { value: 'The Logia: Cloud-Cloud Fruit' }],
    source: '',
    requirements: [{ value: 'Become recognized as the ruler of Angel Island' }, { value: 'Become recognized as the ruler of Upper Yard' }, { value: 'Become recognized as the ruler of angel island' }, { value: 'Maintain your rule for minimum 1 month' }],
    desc: 'Become the \'God\' of Skypiea. To do so you must become the ruler of the three inhabited regions and have no other claimants to the region',
    author: 'Trismegistus',
    loca: 'One Piece',
  },
  {
    rewards: [{ value: 'Grants three times as much of a capture reward.' },
      { value: 'Convince them without offering favors in return, and the reward will double.' }, { value: 'Do this with at least 5 subject and the rewards will double.' }],
    source: '',
    requirements: [{ value: 'Waifu/Husbando is of a personal tier of at least tier 4.' }, { value: 'The subject has to fully understand what subjugation will entail.' }, { value: 'This mission can be accepted a total of 3 times.' }],
    author: 'Ahtu',
    desc: 'Convince Waifu/Husbando\'s of submitting to you willingly. Without any magical means. And taking on your means of subjugation willingly.',
    scope: 'Standard',
    loca: 'Any',
    image: '',
    title: 'When Loyalty Doesn\'t Come Cheap(Deal with the Devil).',
  },
  {
    image: '',
    desc: 'SCP-050 is a small trophy which awards itself to whoever it deems "The Cleverest". If the current owner is deceived, pranked, or otherwise outwitted by any means, it will teleport itself into the possession of the person responsible. You (or a member of your retinue) must earn SCP-050 and keep it for seven consecutive days. Attempting to leave the planet with it will be considered a forfeit. The current owner is Dr. Bright.',
    scope: 'Quick',
    author: 'Nisantis',
    title: 'Great Researcher Prank War of [REDACTED]',
    requirements: [{ value: 'Maintain ownership of SCP-050 for 1 week' }, { value: '050\'s owner must remain on Earth for the duration' }, { value: 'Do not kill or capture Dr. Bright' }],
    rewards: [{ value: '75 credits' }],
    loca: 'SCP',
    source: '',
  },
  {
    rewards: [{ value: '50 Credits' }],
    author: 'Zerlestes',
    title: 'Defense Against Dark arts Teacher',
    requirements: [],
    image: '',
    desc: 'Your Mission is simple Substitute one of the Defense Teachers and Teach at Least until Voldemort is Dead\n\nYour only receive the reward if the Most of your Students pass the O.W.L.S and N.E.W.T.S \n ',
    scope: 'Standard',
    loca: 'Harry Potter',
    source: '',
  },
  {
    scope: 'Standard',
    image: '',
    desc: 'Seduce all girls in RWBY team without using any lures or capture methods.',
    rewards: [{ value: 'Grand quadruple of their capture cost.' }],
    requirements: [{ value: 'Do not use any lures or capture methods on anyone in RWBY team before succesful seducing.' }, { value: 'Do not blackmail or use mind altering powers on anyone in RWBY team before succesful seducing.' }, { value: 'Convince them all to join you as companions on their own free will.' }],
    title: 'Love full of Color',
    loca: 'RWBY',
    author: 'KatzSmile',
    source: '',
  },
  {
    scope: 'Standard',
    requirements: [{ value: 'Teams RWBY and JNPR must survive. Yes that means Pyrrha Nikos must survive too.' }, { value: 'Do not expose your foreknowledge to locals.' }, { value: 'Do not join any sides of conflict directly.' }],
    loca: 'RWBY',
    image: '',
    source: '',
    desc: 'Prevent fall of Beacon without revealing to locals anything related to canon or joining any side directly.',
    title: 'Dust to Dust',
    author: 'KatzSmile',
    rewards: [{ value: '100 Credits' },
      { value: 'Ability to grant and open Aura to anyone' }],
  },
  {
    image: 'https://static.wikia.nocookie.net/fairytail/images/2/26/Fairy_Tail_Banner.png',
    desc: 'Fairy Tail is a guild in Magnolia Town in the Kingdom of Fiore. The name "Fairy Tail" is meant to represent a sense of adventure to learn more about mysteries and destiny. Join the guild and become part of the madness.',
    loca: 'Fairy Tail - Magnolia Town',
    scope: 'Quick',
    requirements: [{ value: 'Become a member of Fairy Tail and get the Fairy Tail Guild Mark.' }],
    source: 'https://fairytail.fandom.com/wiki/Fairy_Tail_(Guild)',
    author: 'Darnoc',
    rewards: [{ value: '12 credits or the Soul Talent + Template Stacking 0 with the power "Magic".' }],
    title: 'Join Fairy Tail',
  },
  {
    scope: 'Standard',
    source: '',
    image: 'https://static.wikia.nocookie.net/fairytail/images/9/97/Current_Fairy_Tail_building.png',
    desc: 'When the core members of Fairy Tail disappeared for 7 years, the guild almost went bankrupt and could not afford the first building of Fairy Tail, so it was foreclosed. Make sure there is enough money and the building does not have to be auctioned.',
    title: 'Save the Fairy Tail Building ',
    author: 'Darnoc',
    rewards: [{ value: '50' }],
    loca: 'Fairy Tail',
    requirements: [{ value: 'Fairy Tail core members have disappeared in the S-class mage advancement exam.' }, { value: 'The building is not auctioned off when the Fairy Tail core members reappear after seven years.' }, { value: 'The building does not have to be auctioned off when the Fairy Tail core members reappear after seven years.' }],
  },
  {
    source: 'https://en.wikipedia.org/wiki/The_Pegasus_(Star_Trek:_The_Next_Generation)',
    rewards: [{ value: '100 Credits' }],
    date: 'Thu Dec 30 2021 12:56:27 GMT+0100 (Mitteleuropäische Normalzeit)',
    loca: 'Star Trek: The Next Generation',
    image: 'https://upload.wikimedia.org/wikipedia/en/1/1c/ST-TNG_The_Pegasus.jpg',
    author: 'Jack87',
    desc: 'Get the Federation Cloaking Device from the USS Pegasus before the USS Enterprise arrives and get out without anybody noticing.',
    scope: 'Quick',
    requirements: [{ value: 'Trace Defense x2' },
      { value: 'Space vehicle with a cloaking device' }],
    title: 'Acquisition',
  },
  {
    rewards: [{ value: 'One tier 9 or lower Nasuverse companion of your choice.' }],
    desc: 'Win a Holy Grail War and claim your wish.',
    scope: 'Standard',
    author: 'V',
    loca: 'Nasuverse',
    source: '',
    title: 'Grail War',
    image: '',
    requirements: [{ value: 'Kill or capture every other Servant.' }, { value: 'Have at least 1 Servant as a companion.' }, { value: 'Keep your chosen servant alive until the end.' }],
  },
  {
    scope: 'Standard',
    author: 'KatzSmile',
    requirements: [{ value: 'Secretly get rid of Voldemort and his inner circle before Voldemort appears in public again or his return became widespread public knowledge.' }],
    source: '',
    loca: 'Harry Potter',
    title: 'He-Who-Must-Be-Forgotten',
    desc: 'Horrors and nightmares must be forgotten. What died must remain dead. Prevent the return of dark times.',
    rewards: [{ value: 'Ability to wield local magic for you and all of your current and future retinue if you dont have one already' }, { value: 'Dumbledore level of mastery of local magic and wizardry for you' }],
    image: '',
  },
  {
    desc: 'Spread awareness or ability in magic or the supernatural through a setting.',
    scope: 'Standard',
    source: '',
    requirements: [{ value: '10% spread' }, { value: '25% spread' }, { value: '50% spread' }, { value: '75% spread' }, { value: '100% spread' }],
    title: 'Magical Awakening',
    image: '',
    author: 'Templar9999',
    loca: 'Any',
    rewards: [{ value: 'Blessed Talent for Free' }, { value: '30% of the world\'s starting budget' }, { value: '30% of the world\'s starting budget' }, { value: '40% of the world\'s starting budget' },
      { value: '100% of the world\'s starting budget' }],
  },
  {
    rewards: [{ value: 'A 1 tier discount on your next companion purchase' },
      { value: 'A free heritage or a heritage upgrade, no T11 options' }, { value: 'Free Siren Song or 100 credits if already purchased' }, { value: 'Free control method upgrade or 100 credits if already fully upgraded' }, { value: '500 credits, world hopping unlocked to bigger fish for free' }],
    loca: 'Any',
    title: 'Vini, Viti, Vici',
    image: '',
    author: 'Templar9999',
    source: '',
    scope: 'Standard',
    requirements: [{ value: 'Establish a stronghold in universe' }, { value: 'Control the surroundings' }, { value: 'Gain in setting allied forces' }, { value: 'Conquer an enemy territory' }, { value: 'Win' }],
    desc: 'Conquer a given setting. If this mission is taken, you may not leave the setting until it is complete.\n\nSetting is defined as the general area a work takes place in. \n Whether it is a nation, planet, or galaxy, depends on the work in question.\nIE: The setting of Harry Potter is Magical Europe\nThe setting of Star Wars is the Galaxy',
  },
]

export const newMissions = [
  {
    image: '',
    reward: '2 credit for every species captured ',
    title: 'Gotta rift them all',
    conditions: [{ value: 'Excluding any company bought locations you may not leave the rifts multiverse. Doing so is considered voluntary completion of of this mission and it may not be taken again.' }],
    desc: 'The company is in need of refreshing their stockpile of species from the rifts Multiverse and it\'s your job to get more. For every species you capture you will be rewarded.',
    source: '',
    scope: 'Quick',
    author: 'N/A',
    objectives: [],
    loca: 'Rifts',
  },
  {
    author: 'Templar9999 ',
    source: '',
    conditions: [{ value: 'No companions available to purchase' }, { value: 'No dimensional travel' }, { value: 'No sweet home' }],
    image: '',
    title: 'Hell Bound',
    loca: 'Any',
    desc: 'You are trapped in Hell.\nYou must escape.\n\nHell is defined as any appropriate hell dimension in the given setting. This includes Tartarus, Nifelheim, etc.',
    scope: 'Standard',
    objectives: [],
    reward: 'Exit Stage Left, and all canon upgrades for free. Sweet Home and all upgrades to Lifes a Beach for free',
  },
  {
    title: 'Is it wrong to join a family.',
    scope: 'Standard',
    source: '',
    reward: '20 credits',
    desc: 'Danmachi is a world full of wonders where many gods descended the heavens to form families of adventurers and defy the dungeon.\nJoin the Family and become defy the dungeon yourself.\n\n',
    conditions: [{ value: 'Join a family and reach the 5th floor of the dungeon.' }],
    objectives: [{ reward: '50 points + one skill or abilites that the you witnessed', value: 'Kill a Jack Bird and bring the Jack Bird\'s Golden Egg\tback.' }, { value: 'Reach the 10th floor of the dungeon.', reward: '50 points' }, { reward: 'Choose Body, Soul or Science Talent for free.', value: 'Reach level 2' }],
    author: 'Irugaa',
    loca: 'Danmachi',
    image: '',
  },
  {
    title: 'Help liberate Naboo',
    source: 'https://starwars.fandom.com/wiki/Invasion_of_Naboo',
    objectives: [{ reward: 'The Lucrehulk-class Droid Control Ship becomes a "Ride" and you get the Ride Perk "Catch-A-Ride Crew Training Program".', value: 'Capture the Lucrehulk-class Droid Control Ship, disable the droid army, and successfully claim the ship for yourself. Can be done by a Companion.' }, { reward: 'An "OC Donut Steel" made by a Jedi of your choice for you or a Companion. Alternatively 15 credits.', value: 'Defeat Darth Maul without killing Qui-Gon Jinn and Obi-Wan Kenobi. Can be done by a Companion.' }],
    reward: '50 Credits',
    loca: 'Star Wars - Prequel Trilogy',
    conditions: [{ value: 'The Trade Federation has occupied Naboo.' }, { value: ' Naboo was liberated from Trade Federation occupation and you helped.' }, { value: 'Padme Amidala lives ' }],
    scope: 'Standard',
    author: 'Darnoc',
    desc: 'The Trade Federation has occupied Naboo.\nHelp liberate Naboo from the Trade Federation occupation and make sure Queen Amidala survives.',
    image: 'https://static.wikia.nocookie.net/starwars/images/a/a2/DroidControlShip-TPM.png',
  },
  {
    conditions: [{ value: 'Must fight for any of the sides.' }, { value: 'Must fight for a minimum of 6 Months. ' }],
    objectives: [{ reward: '50 credits + All pokemons currently owned. ', value: 'Fight the entire war (Variable - Maximum 1 Year.)' }, { reward: '10 - 100 credits', value: 'Receive War Merits' }, { reward: '25 credits per Gym Leader', value: 'Kill a Captain (Gym Leader) of an enemy region ' }, { reward: '200 credit + 20% discount in any Waifu/Husbando', value: 'Kill all Captains (Gym Leader) of an enemy region ' }, { reward: '100 credits ', value: 'Kill a Elite 4 member of a enemy region.' }, { value: 'Kill all Elite 4 members of a enemy region.', reward: '150 credits + 40% discount' }, { reward: '500 credits + 20 discount', value: 'Kill the champion of a enemy region' }, { value: 'Kill all the champion of a enemy region', reward: '2000 credits + 50% discount + Any tier 6 or Lower Waifu/Husbando' }],
    image: '',
    reward: 'Any 3 pokemons of choice + 250 Credits.',
    desc: 'Support Kanto/Johto, Hoenn, Sinnoh, Unova, or Kalos, for the duration of the war.',
    loca: 'Pokemon',
    author: 'Irugaa0',
    title: 'Regional Wars',
    source: '',
    scope: 'Grand',
  },
  {
    source: '',
    desc: 'Company was commissioned to relocate a small town soon to suffer a major disaster in 30 days. You will be responsible for creating new housing on land that you own or that is unclaimed by any nearby governments. To succeed you must create enough infrastructure for at least 1,000 people to become a self sufficient town. Once housing is finished you may request a portal to link the village to your new settlement for 7 days. Once the portal is closed the number of residents that have settled into your town will be counted to determine success. If the population is below 1,000 people you fail the mission and get no rewards except the reactions of the survivors.',
    objectives: [{ value: 'Transport as many people from the village as possible.', reward: '1 of [30 Credits, Copper Waifu Ticket] per 500 people transported over the 1000 person minimum to a limit of 3000 total population.' }, { value: 'Convince the Village to swear fealty/loyalty to yourself.', reward: '+1 of [3 Blue Waifu tickets, 13 credits]' }],
    loca: 'Any',
    title: 'Town Relocation',
    image: '',
    conditions: [{ value: 'Countdown Clock: The longer the building process takes the fewer people will remain. In 30 days the disaster will peak leaving fewer than 1,000 survivors and failing the mission.' }],
    reward: '+1 of [Life\'s a Beach and 2 levels of Sweet Home Expansion. If you already possess Life\'s a Beach then it instead becomes 3 purchases of Sweet Home Expansion, 100 Credits]',
    scope: 'Standard',
    author: '13rett',
  },
  {
    image: 'https://static.wikia.nocookie.net/gameofthrones/images/5/51/Ramsey_about_to_be_eaten.jpg',
    author: 'Darnoc',
    loca: 'A Song of Ice and Fire / Westeros',
    source: '',
    scope: 'Quick',
    desc: 'Kill Ramsay Snow/Bolton or make sure he dies. \nCan be done by a Companion.',
    objectives: [{ value: 'Kill Ramsay Snow/Bolton or make sure he dies before Domeric Bolton dies.', reward: '5 Credits' }, { value: 'Kill Ramsay Snow as a Substitute or Possess of Domeric Bolton.', reward: '8 Credits or Body Defense for free' }],
    title: 'Kill Ramsay Snow/Bolton',
    conditions: [{ value: 'Ramsay Snow/Bolton is Death' }],
    reward: '12 Credits or the Template Stacking 1 "Skinchanger"',
  },
  {
    title: 'Blood Beneath the Bay',
    conditions: [{ value: 'No bound companions of any kind may be brought on this mission.' }, { value: 'No new companions of any kind may be bound or bought on this mission.' }],
    desc: '[Period: Before the Saint\'s War]\nA Skaenite cult in Defiance Bay are plotting to disrupt trade into the Free Palatinate of the Dyrwood, and therefore destabilize the control of its ruling elite, by bringing its capital city and largest port to it\'s knees. This has been determined to run counter to the interests of the Company.\n\nObjectives:\n-Prevent the activation of the Engwithan Machine in Heritage Hill.\n-Sunder the Skaenite Blood Pool underneath the slums of Ondra\'s Gift.\n-Mitigate the armed Skaenite uprising to the point that the Free Palatinate and allied forces will be able to manage it.',
    objectives: [{ value: 'Do not destroy, or allow to be destroyed, the Engwitgan machine in Heritage Hill.', reward: '15 points; or 500 g one-time supply of Luminous Adra, plus 50 gram supply of Luminous Adra recurrent monthly.' }, { value: 'Either scatter or harvest the drowned souls beneath Ondra\'s Gift using the Blood Pool.', reward: '20 points (scatter); or 60 points (harvest)' }, { reward: '30 points', value: 'Personally slay and harvest the Effigy of Skaen made for the uprising.' }],
    scope: 'Standard',
    reward: '60 points',
    source: '',
    image: '',
    loca: 'Pillars of Eternity/Defiance Bay',
    author: 'Zendrelax',
  },
  {
    reward: '200 points will be given for accepting this mission, additional 200 points will be given after completing it',
    conditions: [{ value: 'You must win the grail war, and make a wish to the grail/ You can only bring 7 Companion/ Initial tier of your companion must not exceed 7/ your tier must not exceed 7' }],
    loca: 'Nasuverse',
    author: 'Raaa',
    source: '',
    image: '',
    desc: '1 week before your arrival, all participant in nearest incoming grail war will be warned of your power, purpose, and what means you have. \nThis warning will come as hazy prophecy that they will take seriously. \nHow much they will know will depend on how long they will survive in the \'canon\' grail war.\n A character that survive to the end will understand about 75% of this prophecy.\n\nIn the grail war itself, you will be treated as a lone master in a new faction, and all of your retinue count as a servant in this grail war',
    objectives: [{ value: 'Kill every master in the grail war', reward: '30 points / master' },
      { reward: '50 points / servant', value: 'Kill every servant in the grail war' }],
    scope: 'Grand',
    title: 'Another Faction',
  },
  {
    source: 'https://onepiece.fandom.com/wiki/Four_Emperors',
    conditions: [{ value: 'Defeat one of the Yonkō/emperors by yourself while he or she is recognized as a Yonkō/emperor.' }, { value: 'Become recognized as the new emperor.' }],
    image: 'https://static.wikia.nocookie.net/onepiece/images/2/23/Four_Emperors_Infobox.png',
    scope: 'Grand',
    title: 'Become a Yonkō/Emperor',
    author: 'Darnoc',
    objectives: [{ value: 'Defeat Big Mom/Charlotte Linlin while she is recognized as a Yonkō/emperor.', reward: '10 Credits or a Template Stacking 1 with the power "Soul-Soul Fruit/Soru Soru no Mi" for you or one Companion.' }, { reward: '10 Credits or a Template Stacking 1 with the power "Fish-Fish Fruit, Model Azure Dragon/Uo Uo no Mi, Model: Seiryu" for you or a companion.', value: 'Defeat Kaidou of the Beasts while he is recognized as a Yonkō/emperor.' }, { reward: '10 Credits or a Template Stacking 1 with the power "Dark-Dark Fruit/Yami Yami no Mi " for you or a companion.', value: 'Defeat Blackbeard/Marshall D. Teach while he is recognized as a Yonkō/emperor.' }, { reward: '10 Credits or a Template Stacking 1 with the power "Tremor-Tremor Fruit/Gura Gura no Mi " for you or a companion.', value: 'Defeat Whitebeard/Edward Newgate while he is recognized as a Yonkō/emperor.' }, { value: 'Defeat Red Hair/Shanks while he is recognized as a Yonkō/emperor.', reward: '10 credits or "Covert Talent" and "Talent Sharing" for "Covert Talent".' }],
    desc: 'The four emperors are the four most notorious and powerful pirate captains in the world.Defeat one of the Yonkō/emperors yourself and be recognized as the new Yonkō/emperor.\nThe corresponding Yonkō/emperor must be defeated by yourself without help. His crew can be defeated by you, your Companions, or your crew.',
    loca: 'One Piece - New World',
    reward: '300 Credits',
  },
  {
    objectives: [{ value: 'Kill Enel or cause him to die.', reward: '10 Credits or Template Stacking 1 with the power "Cloud-Cloud Fruit" for you or one Companion. ' }],
    author: 'Darnoc',
    scope: 'Standard',
    source: '',
    loca: 'One Piece - Sky Island',
    image: 'https://static.wikia.nocookie.net/onepiece/images/a/ad/Enel_Anime_Infobox.png',
    conditions: [{ value: 'Besiege Eneru/Enel.' }],
    desc: 'Defeat Eneru/Enel. Can be done with the help of Companions.',
    reward: '100 Credits',
    title: 'Defeat Eneru/Enel',
  },
  {
    image: '',
    objectives: [{ value: 'Don’t be caught any single time', reward: '20 credits+any tier 5 companion of your choice.' }, { reward: '10 credits.', value: 'Don’t be the catcher when the game ends ' }, { reward: '10 credits.', value: 'Be caught less times than the others.' }],
    desc: 'the high-level contractor Zelretch organized a little game to prepare the new contractors and entertain himself. Play a “catch” game with 5 other contractors.',
    title: 'Zelretch the troll wants entertainment. ',
    scope: 'Standard',
    loca: 'any',
    source: '',
    conditions: [{ value: 'No Waifus/Husbando is allowed. ' }, { value: 'Aura/Magic/ Psychic or any other powers are allowed. ' }, { value: 'Using equipment to catch is allowed. (Ex: sword, chains, ropes.) the catcher must be in contact with the item.' }, { value: 'Killing directly other members is prohibited.' }, { value: 'Can’t leave the designated area.' }, { value: 'Be the catcher when the game ends: Lose half of your credits.' }, { value: 'Be the one caught more times: Lose half of your credits.' }, { value: 'Be the First to be caught: Lose 10 credits.' }, { value: 'Kill directly other members: lose all your credits and be 100 credits in debt for each player.' }],
    author: 'irugaa0',
    reward: '20 credits.',
  },
  {
    loca: 'Dragon Age: inquisition',
    conditions: [{ value: '' }],
    reward: '10 credits.',
    desc: 'Join the inquisition and Survive the first dark spawn attack.',
    image: '',
    scope: 'Grand',
    author: 'irugaa0',
    source: '',
    title: 'The Inquisition',
    objectives: [{ reward: '5 credits.', value: 'Reach Skyhold ' }, { value: 'Capture/kill one advisor or companion.', reward: '+ 50% bonus of the buy value.' }, { value: 'Learn a specialization', reward: '10 credits.' }, { value: 'Kill a high dragon', reward: '40 credits + a talent or defence of your choice' }, { value: 'Kill all 11 high dragons', reward: '200 credits + Dragon Heart of your choice.' }, { value: 'Convince the inquisition to side with the mages', reward: 'Soul talent' }, { value: 'Convince the inquisition to side with the templars', reward: 'Body Talent.' }, { reward: '50 credits + 1 free talent.', value: 'Kill a Archdemon' }, {
      reward: '50 credits + one perk of your choice',
      value: 'Capture/kill Corypheus',
    }],
  },
  {
    source: '',
    author: 'Tactition101',
    reward: '25 Points + Female Luffy for your Retinue',
    title: 'King Luffy Doesn\'t Need A Queen',
    desc: 'Luffy friend zoned four members of royalty: Vivi, Boa, Shirahoshi, and Rebecca. Capture all four for your Retinue/Sell',
    objectives: [{ reward: '+ 5 points per \'kidnapped victim\' Morgan reports.', value: 'Morgans hears about this and news of it winds up in the World Economic Journal' }, { value: 'Capture one or more Celestial Dragon while you\'re at it.', reward: '+ 50 Points per Celestial Dragon you catch.' }],
    scope: 'Standard',
    loca: 'One Piece',
    conditions: [{ value: '' }],
    image: '',
  },
  {
    image: 'https://i.imgur.com/8dkPI4z.jpg',
    loca: 'One Piece',
    author: 'Gol. D. Roger',
    scope: 'Standard',
    reward: '50 Credits',
    conditions: [{ value: '' }],
    objectives: [{ reward: '75 Credits', value: 'Get a bounty of 150,000,000 Beris' }, { reward: '120 Credits', value: 'Get a bounty of 550,000,000 Beris' }],
    desc: 'Get a bounty of 50,000,000 Beris',
    title: 'I am a pirate!',
    source: '',
  },
  {
    image: '',
    objectives: [],
    source: '',
    desc: 'Make every female member (at last one) of a protectorate team pregnant',
    title: 'Mission Honey Cape ',
    reward: '150',
    scope: 'Standard',
    author:
  'Contessa',
    conditions: [{ value: 'Binding and Lures won\'t work on the targets for the duration of the mission' }],
    loca: 'Worm',
  },
  {
    source: '',
    reward: '20 credits + 1 talent of your choice.',
    author: 'irugaa0',
    loca: 'Any',
    title: 'Warfare',
    scope: 'Standard',
    desc: 'The company was commissioned to protect an important fortified city from an enemy attack, You alongside with five other contractors, will be responsible for protecting the city until reinforcement comes. To succeed you must protect the city enough to the reinforcement troops arrive at the city and guarantee that at least half or more of the city population is alive. The city has a total of 600 soldiers at your command. ',
    image: '',
    objectives: [{ value: 'Don\'t let the city be violated', reward: '25 credits' }, { reward: '30 credits + Pocket Space or one upgrade to your home up to Grand Manor', value: 'Guarantee that 90% of population is alive. (4500/4500)' }, { value: 'Guarantee that 80% of population is alive. (4000/4000)', reward: '20 credits or a ride plus upgrades up to the same valor.' }, { reward: 'a Tier-3 Companion of your choice.', value: 'Guarantee that 70% of population is alive. (3500/3500)' },
      { value: 'Defeat the Invaders.', reward: '50 points + Any Talent, Defense or Perk of your choice.' }],
    conditions: [{ value: 'Half of the city population must be alive. (5000/5000)' }, { value: 'Time to reinforcement came. (30) days.' }],
  },
  {
    source: '',
    reward: 'Pocket Space, Pocket Apartment, Life\'s A Beach (by default this will appear as the painted world and may contain a Waifu)',
    scope: 'Quick',
    loca: 'Darksouls 1',
    author: 'Waterfly',
    desc: 'The chosen undead may cross paths with the adorable cross-breed Priscilla, putting her in grave danger of being killed. Your mission will be to PROTECT this manifestation of waifuness. \n',
    conditions: [{ value: 'Do not capture Priscilla until the danger has passed.' }],
    objectives: [],
    title: 'Save the dragon WAIFU',
    image: 'https://i.imgur.com/no9KhWP.jpg',
  },
  {
    desc: 'The Pax Cybertronia ("Peace on Cybertron") can be thought of as the ultimate goal of Transformer civilization: permanently ensuring the stability of Cybertron and her colonies, ending the myriad wars that have plagued the Transformer homeworld, and ushering in an era of harmony, enlightenment, and prosperity for all Cybertronians.\n',
    objectives: [{ reward: 'Gain Optimus Prime and Megatron as companions', value: 'End the Great War peacefully' }, { value: 'Kill Unicron', reward: 'Gain T11 Ticket' }],
    reward: 'Gain your own copy of Cybertron as a home,all non T11 Home perks,1 T11 ticket,',
    image: 'https://tfwiki.net/mediawiki/images2/d/d1/PaxCybertronia.jpg',
    title: 'Pax Cybertronia',
    loca: 'Transformers',
    conditions: [{ value: 'End the Great War' }, { value: 'Have a viable population of cybertronians on cybertron' }, { value: 'Peace must Last for 10 Stellar Cycles 1 Stellar Cycle/400 Earth days' }],
    scope: 'Grand',
    author: 'Everythingman101',
    source: '',
  },
  {
    author: 'HeraldofGaia',
    title: 'The Rumbling ',
    image: '',
    conditions: [{ value: 'Can\'t be above tier 6' }, { value: 'must complete before 13 years have passed from start of cannon' }, { value: 'No retinue members above tier 6 ' }, { value: 'No Binding or lures on titan shifters or Akermans ' }],
    scope: 'Grand',
    desc: 'Cause the rumbling using the Founding titan\'s power and eliminate all life outside the island of Paradis ',
    objectives: [],
    reward: 'Exit Stage Left and all upgrades + 300 credits',
    loca: 'Attazk on Titan',
    source: '',
  },
  {
    desc: 'Hogwarts School of Witchcraft and Wizardry, often abbreviated as Hogwarts, is a British wizarding school in the Scottish Highlands.\nChildren with magical abilities are enrolled from birth, and admission was confirmed by owl post at the age of eleven. However, if the child was a Muggle child or a half-blood like Harry Potter, who knew nothing of the wizarding world, a member of the school staff would visit the child and his family to inform them of their magical heritage and the existence of the wizarding world. Become a Hogwarts student yourself and learn about magic.',
    loca: 'Harry Potter - Hogwarts',
    image: 'https://static.wikia.nocookie.net/harrypotter/images/a/ae/Hogwartscrest.png',
    source: 'https://harrypotter.fandom.com/wiki/Hogwarts_School_of_Witchcraft_and_Wizardry',
    conditions: [],
    objectives: [{ reward: '7 credits or the "Performance Talent".', value: 'Get assigned to Gryffindor by the Sorting Hat and join the House of Lions.' }, { value: 'Get assigned to Hufflepuff by the Sorting Hat and join the House of Badgers.', reward: '7 credits or the "Communication Talent".' }, { reward: '7 credits or the "Aesthetic Talent".', value: 'Become assigned to Ravenclaw by the Sorting Hat and join the House of Eagles.' }, { reward: '7 credits or the "Covert Talent".', value: 'Get assigned to Slytherin by the Sorting Hat and join the House of Snakes.' }],
    title: 'Schooltime',
    scope: 'Quick',
    reward: '7 Credits or 1 Creature Defense ( you will need it there:) )',
    author: 'Darnoc',
  },
  {
    author: 'Darnoc',
    desc: 'In his first attempt to circumvent the prophecy, Voldemort, when Harry was one year and three months old, murdered Harry\'s parents who were trying to protect him. Lily\'s loving sacrifice led to the unsuccessful attempt to kill Harry and Voldemort\'s first downfall. This downfall marked the end of the First Wizarding War. According to the prophecy, this attempt to kill Harry also made him, rather than Neville, Voldemort\'s archenemy.\n\nAnother consequence of Lily\'s sacrifice was that her orphaned son had to be raised by her only remaining blood relative, his Muggle aunt Petunia Dursley. However, due to Petunia\'s dislike of her sister and her sister\'s magical gifts, Harry grew up abused and neglected. In her care, however, he was protected from Lord Voldemort by the blood spell Albus Dumbledore placed on him until he came of age or no longer lived in his aunt\'s house.\n\nMake sure he lives in a better environment, either by living in a better place for him or by making sure Petunia treats him better.',
    scope: 'Standard',
    reward: '50 Credits',
    title: 'Harry Potter\'s Home Life.',
    source: 'https://harrypotter.fandom.com/wiki/Harry_Potter',
    objectives: [{ reward: '8 Credits or the "Soul Talent" or "Talent Sharing" for "Soul Talent".', value: 'Make sure Harry Potter continues to be protected with the Blood Bond spell.' }, { reward: '8 Credits or the "Communication Talent" or "Talent Sharing" for "Communication Talent".', value: 'Make sure that Harry Potter is treated better by Petunia.' }],
    loca: 'Harry Potter',
    image: '',
    conditions: [{ value: 'Harry Potter lives in a better environment.' }],
  },
  {
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Pleurodeles_waltl_BUD.jpg/1280px-Pleurodeles_waltl_BUD.jpg',
    source: 'https://harrypotter.fandom.com/wiki/Nastily_Exhausting_Wizarding_Test',
    title: 'Nastily Exhausting Wizarding Test',
    conditions: [{ value: 'You are a Hogwarts student.' }],
    reward: '40 Credits',
    objectives: [{ value: 'Pass all your subjects with at least Exceeds Expectations.', reward: '15 Credits' }, { reward: '15 Credits', value: 'Pass all your subjects with at least Outstanding.' }],
    scope: 'Standard',
    author: 'Darnoc',
    desc: 'A Nastily Exhausting Wizarding Test (often abbreviated N.E.W.T.) was a subject-specific exam that seventh-year witches and wizards at Hogwarts School of Witchcraft and Wizardry took to help them pursue certain careers after their graduation, so passing these exams was critical. \nPass all your subjects with at least Acceptable .',
    loca: 'Harry Potter - Hogwarts',
  },
  {
    loca: 'Harry Potter - Hogwarts',
    desc: 'An Ordinary Wizarding Level (often abbreviated to O.W.L.) is a standardized subject-specific test taken during the fifth year of school at Hogwarts. The score a student achieved on a particular O.W.L. determined whether or not they could continue taking the subject in subsequent school years. \nPass all your subjects with at least Acceptable.',
    objectives: [{ reward: '10 Credits', value: 'Pass all your subjects with at least Exceeds Expectations.' }, {
      reward: '10 credits or "Soul Talent" or"Talent Sharing" for "Soul Talent".',
      value: 'Pass all your subjects with at least Outstanding.',
    }],
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Strix-varia-005.jpg/1280px-Strix-varia-005.jpg',
    author: 'Darnoc',
    reward: '30 Credits',
    conditions: [{ value: 'You are a Hogwarts student.' }],
    scope: 'Standard',
    title: 'Ordinary Wizarding Level ',
    source: 'https://harrypotter.fandom.com/wiki/Ordinary_Wizarding_Level',
  },
  {
    conditions: [{ value: 'Must start with yourself and companions at the same tier as or lower than the Diablo 3 heroes.' }, { value: 'Can be completed with multiple timelines and duplicates if necessary for personality, death, etc.' }, { value: '' }],
    objectives: [{ value: 'Use any technique such as \'Third Generation Symbiote\' etc. to merge with all of your acquired quest targets for at least a year.', reward: 'Immediately ascend to T11 as you have become like unto Anu before they split themselves.' }, { value: 'Complete the quest with at least \'X\' Diablo hero/s greatly involved.', reward: 'Any one discounted defense level per hero involved. Or 50 points per hero, max 1000.' }, { value: 'Enter Sanctuary far enough back to capture the Worldstone, and bring it into your Demiplane, then purify it by any means.', reward: 'Your Demiplane can now expand automatically without limit, gain one free purchase of Sweet Home Expansion per 5 worlds you complete. If you are T11 the 25 purchase cap is removed and new celectial bodies will appear in your solar system/universe naturally depending on size.' }],
    author: 'dikdik1238',
    reward: 'Heaven and Hell merge with your Demiplane however you wish them to. They can act as afterlives to you and your companions, and anyone who has prayed to you or cursed your name.',
    loca: 'Diablo',
    title: 'The new Dawn',
    scope: 'Grand',
    source: '',
    desc: 'Capture and unite at least 5 of the Great Evils and 5 of the Archangels under your single banner, and have them be at least grudgingly accepting of this. They absolutely despise each-other, obviously; getting them to live together will take massive charisma, mind manipulation, possibly dog training experience, and greater willpower than a shonen protagonist. I recommend several purchases of anthropize as well. Good Luck.',
    image: '',
  },
]
